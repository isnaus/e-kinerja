package khasanah.isna.e_kinerja


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_dokumen.*
import kotlinx.android.synthetic.main.activity_signin.*
import kotlinx.android.synthetic.main.activity_tambah_kinerja.*
import kotlinx.android.synthetic.main.row_tampil_data.*
import java.util.*

class TambahKinerjaActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var db : FirebaseFirestore
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter : SimpleAdapter

    lateinit var uri : Uri
    val COLLECTION = "kinerja"
    val F_ID = "id"
    //    val F_NAME = "file_name"
    val F_NAME_KINERJA = "name"
    val F_KETERANGAN = "keterangan"
    val F_CATATAN = "catatan"
    val F_OUTPUT = "output"
    val F_JUMLAH = "jumlah"
    val F_WAKTU_AWAL = "waktu_awal"
    val F_WAKTU_AKHIR = "waktu_Akhir"
    val F_TANGGAL = "tanggal"
    val F_TEMPAT = "tempat"
//    val F_TYPE = "file_type"
//    val F_URL = "file_url"
//    val RC_OK = 100
//    var fileType =""
//    var fileName =""


    val arrayOutput = arrayOf("Foto", "Word", "PDF", "Video")
    lateinit var adapterSpin1: ArrayAdapter<String>

    var jamawal = 0
    var menitawal = 0

    var jamakhir = 0
    var menitakhir = 0

    var tahun = 0
    var bulan = 0
    var tanggal = 0

    private lateinit var tvData1: TextView
    private lateinit var tvData2: TextView

    var docId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_kinerja)
        lsdata.setOnItemClickListener(itemClick)


        tvData1 = findViewById(R.id.txSelected)
        if (intent.extras != null) {
            tvData1.setText(intent.getStringExtra("data1"))
        }
        tvData2 = findViewById(R.id.txMaps)
        if (intent.extras != null) {
            tvData2.setText(intent.getStringExtra("data2"))
        }



        adapterSpin1 = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayOutput)
        sp_output.adapter = adapterSpin1
        sp_output.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext, "Tidak ada yg dipilih", Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                Toast.makeText(baseContext, adapterSpin1.getItem(position), Toast.LENGTH_SHORT)
                    .show()
            }
        }

        btn_Dokumen.setOnClickListener(this)
        btn_maps.setOnClickListener(this)
        btn_simpan_kinerja.setOnClickListener(this)
        bt_kirim.setOnClickListener(this)
        logout.setOnClickListener(this)
        alFile = ArrayList()
        uri = Uri.EMPTY

        tx_waktu_awal.setOnClickListener {
            val tpd = TimePickerDialog(
                this,
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    // Display Selected date in Toast
                    tx_waktu_awal.text = String.format("%02d:%02d", hourOfDay, minute)

                },
                jamawal,
                menitawal,
                true
            )
            tpd.show()
        }
        tx_waktu_akhir.setOnClickListener {
            val tpd = TimePickerDialog(
                this,
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    // Display Selected date in Toast
                    tx_waktu_akhir.text = String.format("%02d:%02d", hourOfDay, minute)

                },
                jamakhir,
                menitakhir,
                true
            )
            tpd.show()
        }
        val cal: Calendar = Calendar.getInstance()

        bulan = cal.get(Calendar.MONTH)
        tahun = cal.get(Calendar.YEAR)
        tanggal = cal.get(Calendar.DAY_OF_MONTH)

        tx_tanggal.setOnClickListener {
            val dpd = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    tx_tanggal.text = "$year-${monthOfYear + 1}-$dayOfMonth"
                    tahun = year
                    bulan = monthOfYear
                    tanggal = dayOfMonth

                },
                tahun,
                bulan,
                tanggal
            )
            dpd.show()
        }
    }


    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener{querySnapshot, e ->
            //if(e !=null) e.message?.let { Log.d("firestore", it) }
            showData()
        }
    }

    fun showData() {
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alFile.clear()
            for (doc in result) {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, doc.get(F_ID).toString())
                hm.set(F_NAME_KINERJA, doc.get(F_NAME_KINERJA).toString())
                hm.set(F_KETERANGAN, doc.get(F_KETERANGAN).toString())
                hm.set(F_CATATAN, doc.get(F_CATATAN).toString())
                hm.set(F_OUTPUT, doc.get(F_OUTPUT).toString())
                hm.set(F_JUMLAH, doc.get(F_JUMLAH).toString())
                hm.set(F_WAKTU_AWAL, doc.get(F_WAKTU_AWAL).toString())
                hm.set(F_WAKTU_AKHIR, doc.get(F_WAKTU_AKHIR).toString())
                hm.set(F_TANGGAL, doc.get(F_TANGGAL).toString())
                hm.set(F_TEMPAT, doc.get(F_TEMPAT).toString())
                alFile.add(hm)
            }
            adapter = SimpleAdapter(
                this, alFile, R.layout.row_tampil_data,
                arrayOf(F_ID, F_NAME_KINERJA, F_KETERANGAN, F_CATATAN, F_OUTPUT, F_JUMLAH, F_WAKTU_AWAL, F_WAKTU_AKHIR, F_TANGGAL, F_TEMPAT),
                intArrayOf(R.id.txId, R.id.txnama, R.id.txketerangan, R.id.txcatatan, R.id.txoutput, R.id.txjumlah, R.id.txwaktuawal, R.id.txwaktuakhir, R.id.txtanggal,
                    R.id.txtempat)
            )
            lsdata.adapter = adapter
        }
    }

    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
        val hm = alFile.get(position)
        docId = hm.get(F_ID).toString()
        edId.setText(docId)
        ed_nama_kinerja.setText(hm.get(F_NAME_KINERJA).toString())
        ed_keterangan.setText(hm.get(F_KETERANGAN).toString())
        ed_catatan.setText(hm.get(F_CATATAN).toString())
        txoutput.setText(hm.get(F_OUTPUT).toString())
        txjumlah.setText(hm.get(F_JUMLAH).toString())
        tx_waktu_awal.setText(hm.get(F_WAKTU_AWAL).toString())
        tx_waktu_akhir.setText(hm.get(F_WAKTU_AKHIR).toString())
        tx_tanggal.setText(hm.get(F_TANGGAL).toString())
        txMaps.setText(hm.get(F_TEMPAT).toString())
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_Dokumen -> {
                var intent = Intent(this, DokumenActivity::class.java)
                startActivity(intent)
            }
            R.id.btn_maps -> {
                var intent = Intent(this, MapsActivity::class.java)
                startActivity(intent)
            }
            R.id.logout -> {
                var intent = Intent(this, SignInActivity::class.java)
                startActivity(intent)
            }
            R.id.btn_simpan_kinerja -> {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, edId.text.toString())
                hm.set(F_NAME_KINERJA, ed_nama_kinerja.text.toString())
                hm.set(F_KETERANGAN, ed_keterangan.text.toString())
                hm.set(F_CATATAN, ed_catatan.text.toString())
                hm.set(F_OUTPUT, sp_output.selectedItemPosition)
                hm.set(F_JUMLAH, ed_jumlah.text.toString())
                hm.set(F_WAKTU_AWAL, tx_waktu_awal.text.toString())
                hm.set(F_WAKTU_AKHIR, tx_waktu_akhir.text.toString())
                hm.set(F_TANGGAL, tx_tanggal.text.toString())
                hm.set(F_TEMPAT, txMaps.text.toString())

                db.collection(COLLECTION).document(edId.text.toString()).set(hm).addOnSuccessListener {
                    Toast.makeText(this, "Data successfully added", Toast.LENGTH_SHORT)
                        .show()

                }.addOnFailureListener { e ->
                    Toast.makeText(this, "Data unsuccessfully added :${e.message}", Toast.LENGTH_SHORT)
                        .show()

                }
            }
            R.id.bt_kirim -> {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, docId)
                hm.set(F_ID, edId.text.toString())
                hm.set(F_NAME_KINERJA, ed_nama_kinerja.text.toString())
                hm.set(F_KETERANGAN, ed_keterangan.text.toString())
                hm.set(F_CATATAN, ed_catatan.text.toString())
                hm.set(F_OUTPUT, sp_output.selectedItemPosition)
                hm.set(F_JUMLAH, ed_jumlah.text.toString())
                hm.set(F_WAKTU_AWAL, tx_waktu_awal.text.toString())
                hm.set(F_WAKTU_AKHIR, tx_waktu_akhir.text.toString())
                hm.set(F_TANGGAL, tx_tanggal.text.toString())
                hm.set(F_TEMPAT, txMaps.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this,"Data succesfully updated",Toast.LENGTH_SHORT)
                            .show() }
                    .addOnFailureListener {e ->
                        Toast.makeText(this, "Data Unsuccesfully update : ${e.message}",Toast.LENGTH_SHORT)
                            .show()
                    }
            }
        }

    }
}






