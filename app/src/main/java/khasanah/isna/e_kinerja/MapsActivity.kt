package khasanah.isna.e_kinerja

import android.content.Intent
import android.location.Geocoder
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_dokumen.*
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.activity_tambah_kinerja.*
import mumayank.com.airlocationlibrary.AirLocation

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, View.OnClickListener {

    private lateinit var etData2 : EditText

    var airLoc: AirLocation? = null
    var gMap: GoogleMap? = null
    lateinit var mapFragment: SupportMapFragment

    var knj_lat: Double = 0.0
    var knj_lng: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        mapFragment = supportFragmentManager.findFragmentById(R.id.fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fab.setOnClickListener(this)
        bt_maps_simpan.setOnClickListener(this)
        etData2 = findViewById(R.id.editText)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        airLoc?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        airLoc?.onRequestPermissionsResult(requestCode, permissions, grantResults)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onMapReady(p0: GoogleMap?) {
        gMap = p0
        if (gMap != null) {
            airLoc = AirLocation(this, true, true,
                object : AirLocation.Callbacks {
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(
                            this@MapsActivity,
                            "Gagal mendapatkan lokasi...",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onSuccess(location: Location) {
                        val ll = LatLng(location.latitude, location.longitude)
                        var latitude = location.latitude
                        var longitude = location.longitude
                        gMap!!.addMarker(MarkerOptions().position(ll).title("Posisi Anda Sekarang"))
                        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, 16.0f))

                        knj_lat = latitude
                        knj_lng = longitude
                        val address = getAddress(latitude, longitude)
                        editText.setText(address)
                    }

                })
        }
    }

    fun getAddress(lat: Double, lng: Double): String {
        val geocoder = Geocoder(this)
        val list = geocoder.getFromLocation(lat, lng, 1)
        return list[0].getAddressLine(0)
    }

    override fun onClick(v: View?) {
        if (gMap != null) {
            airLoc = AirLocation(this, true, true,
                object : AirLocation.Callbacks {
                    override fun onFailed(locationFailedEnum: AirLocation.LocationFailedEnum) {
                        Toast.makeText(
                            this@MapsActivity,
                            "Gagal mendapatkan lokasi...",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onSuccess(location: Location) {
                        val ll = LatLng(location.latitude, location.longitude)
                        var latitude = location.latitude
                        var longitude = location.longitude
                        gMap!!.addMarker(MarkerOptions().position(ll).title("Posisi Anda Sekarang"))
                        gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, 16.0f))

                        knj_lat = latitude
                        knj_lng = longitude
                        val address = getAddress(latitude, longitude)
                        editText.setText(address)
                    }

                })
        }
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when (v?.id) {
            R.id.bt_maps_simpan -> {
                val intent = Intent(this, TambahKinerjaActivity::class.java)
                intent.putExtra("data2", etData2.text.toString())
                startActivity(intent)
                finish()
            }

        }
    }
}

